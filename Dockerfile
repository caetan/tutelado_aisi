FROM python:3.9.4-buster

WORKDIR /opt

RUN apt-get update && apt-get install -y --no-install-recommends
COPY Pipfile Pipfile.lock entrypoint.sh config.py ./
COPY ./app ./app
RUN pip install pipenv && pipenv install --system --deploy --ignore-pipfile && chmod -R 777 entrypoint.sh

EXPOSE 8080

WORKDIR /opt/app
ENTRYPOINT ["../entrypoint.sh"]
#CMD ["/bin/bash", "-c", "while true; do sleep 30; done;"]
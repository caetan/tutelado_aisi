###################  Docker development directives  ####################
#
# Usage:
# make env              # build environment, create and start containers
# make build-env		# force rebuild image and environment
# make build			# build image for GitLab
# make push				# push image to GitLab
# make install			# install Helm chart
# make uninstall		# uninstall Helm chart
# make stop				# stops containers
# make clean			# removes container, network and volume

env:
	docker-compose up --force-recreate --remove-orphans
.PHONY: env

build-env:
	docker-compose build --force-rm
	make env
.PHONY: stop

build:
	docker build -t registry.gitlab.com/caetan/tutelado_aisi/myapp .
.PHONY: build

push:
	docker push registry.gitlab.com/caetan/tutelado_aisi/myapp
.PHONY: push

install:
	helm install tutelado charts/charts/myapp
.PHONY: install

uninstall:
	helm uninstall tutelado
.PHONY: uninstall

stop:
	docker-compose stop
.PHONY: stop

clean:
	docker-compose down -v
.PHONY: clean

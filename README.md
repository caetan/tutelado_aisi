## Traballo tutelado

# How to run locally

```
make env
```

Access app http://localhost:8080

Query DB http://localhost:8080/db


# MiniShift deployment

## Build image into GitLab repo
```
make build
```

## Push image to GitLab
```
make push
```

## Install Helm chart
```
 make install	
```

## Uninstall Helm chart
```
 make uninstall	
```

Once chart is installed and app is deployed it's necessary to create a route to access it.

Service access: http://tutelado-aisi.192.168.99.101.nip.io
